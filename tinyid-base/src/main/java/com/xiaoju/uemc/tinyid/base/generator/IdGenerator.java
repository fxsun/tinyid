package com.xiaoju.uemc.tinyid.base.generator;

import java.util.List;

/**
 * @author du_imba
 */
public interface IdGenerator {
    /**
     * get next id
     * @return
     */
    Long nextId();

    Long nextIdBizId();

    /**
     * get next id batch
     * @param batchSize
     * @return
     */
    List<Long> nextId(Integer batchSize);

    List<Long> nextIdBizId(Integer batchSize);

}
