package com.xiaoju.uemc.tinyid.client;

import com.xiaoju.uemc.tinyid.client.utils.TinyId;

public class Test {

    @org.junit.Test
    public void test1() {
        for (int i = 0; i < 10; i++) {
            Long id = TinyId.nextId("test_odd");
            Long idBiz = TinyId.nextIdBizId("test_odd");
            String id3 = String.valueOf(System.currentTimeMillis() + "" + idBiz);
            System.out.println("current id is: " + id + " length:" + id.toString().length());
            System.out.println("current id is: " + idBiz + " length:" + idBiz.toString().length());
            System.out.println("current id is: " + id3 + " length:" + id3.toString().length());
        }
    }

    @org.junit.Test
    public void test2() {
        System.out.println("1159903982616553362136");
        System.out.println("1159903982616553362136".length());
        Long id = TinyId.nextIdBizId("test");
        StringBuffer str = new StringBuffer();
        str.append(System.currentTimeMillis()).append(id);
        System.out.println(str);
        System.out.println(str.length());
    }

    @org.junit.Test
    public void test3() {
        while(true) {
            Long id = TinyId.nextId("test");
            Long id2 = TinyId.nextIdBizId("test");
            StringBuffer str = new StringBuffer();
            System.out.println(str + "\t" + str.length());
            str.append(System.currentTimeMillis()).append(id);
            System.out.println(str + "\t" + str.length());
        }
    }

}
